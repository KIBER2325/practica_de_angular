import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { PricesComponent } from './components/prices/prices.component';
import { USUARIO_ROUTES } from './components/prices/prices-routes';

const routes: Routes = [
  {path:'home', component:HomeComponent},
  {path:'prices/:id',component:PricesComponent,children:USUARIO_ROUTES},

  {path:'about',component:AboutComponent},
  {path:'**', pathMatch:'full', redirectTo:'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
