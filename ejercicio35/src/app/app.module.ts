import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HomeComponent } from './components/home/home.component';
import { PricesComponent } from './components/prices/prices.component';
import { Ruta20Component } from './components/prices/ruta20.component';
import { Ruta50Component } from './components/prices/ruta50.component';
import { Ruta80Component } from './components/prices/ruta80.component';
import { AboutComponent } from './components/about/about.component';
import { DiasComponent } from './components/prices/dias/dias.component';
import { DomingoComponent } from './components/prices/dias/domingo.component';
import { LunesComponent } from './components/prices/dias/lunes.component';
import { MartesComponent } from './components/prices/dias/martes.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HomeComponent,
    PricesComponent,
    Ruta20Component,
    Ruta50Component,
    Ruta80Component,
    AboutComponent,
    DiasComponent,
    DomingoComponent,
    LunesComponent,
    MartesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
