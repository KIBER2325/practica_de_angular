import { Routes } from "@angular/router";
import { ROUTAS_F2 } from "./ruta20-routes";
import { Ruta20Component } from "./ruta20.component";
import { Ruta50Component } from "./ruta50.component";
import { Ruta80Component } from "./ruta80.component";


export const USUARIO_ROUTES : Routes=[
{path:'ruta20/:parametros',component:Ruta20Component,children:ROUTAS_F2},
{path:'ruta50',component:Ruta50Component},
{path:'ruta80',component:Ruta80Component},
{path:'**', pathMatch:'full',redirectTo:'ruta20'}
];

