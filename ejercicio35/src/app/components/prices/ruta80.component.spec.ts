import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Ruta80Component } from './ruta80.component';

describe('Ruta80Component', () => {
  let component: Ruta80Component;
  let fixture: ComponentFixture<Ruta80Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Ruta80Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Ruta80Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
