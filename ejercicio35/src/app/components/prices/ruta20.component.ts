/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ruta20',
  templateUrl: './ruta20.component.html',
  styleUrls: ['./ruta20.component.css']
})
export class Ruta20Component implements OnInit {

  constructor(private routes:ActivatedRoute) {
    this.routes.parent?.params.subscribe(parametros =>{
      console.log('ruta hija - ruta20');
      console.log(parametros);
      });

   }

  ngOnInit(): void {
  }

}
