import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Ruta50Component } from './ruta50.component';

describe('Ruta50Component', () => {
  let component: Ruta50Component;
  let fixture: ComponentFixture<Ruta50Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Ruta50Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Ruta50Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
