import { Routes } from "@angular/router";
import { DomingoComponent } from "./dias/domingo.component";
import { LunesComponent } from "./dias/lunes.component";
import { MartesComponent } from "./dias/martes.component";

export const ROUTAS_F2 :Routes = [
    {path:'domingo',component:DomingoComponent},
    {path:'lunes',component:LunesComponent},
    {path:'martes',component:MartesComponent},
    {path:'**', pathMatch:'full',redirectTo:'domingo'}

]