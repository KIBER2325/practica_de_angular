/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent implements OnInit {
  numero :number = 0;
  advertencia:any = "no es posible sobresar los limites";
  contador:number =0;
  constructor() { }

  ngOnInit(): void {
  }

incrementar(){
  if (this.contador>=0 && this.numero<50) {
    this.numero++;
  } this.contador++
 
}
decrementar(){
  if (this.contador>0 && this.contador<=50) {
    this.numero--;
  }
  this.contador--;
}


 }


