import { Component, OnInit } from '@angular/core';
import { tipo1,tipo2, tipo3 } from 'src/app/interfaces/interface';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-selectores',
  templateUrl: './selectores.component.html',
  styleUrls: ['./selectores.component.css'],
  providers: [DataService]
})
export class SelectoresComponent implements OnInit {

  constructor(private DataSvw : DataService) { }

  ngOnInit(): void {
    console.log(this.DataSvw.getTipoOne());
    
  }

}
