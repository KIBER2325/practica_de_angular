import { Injectable } from '@angular/core';
import { tipo1,tipo2,tipo3 } from '../interfaces/interface';

@Injectable()
export class DataService {
 private tipo_1 : tipo1 [] = [
   {
   id:1,
   nombre:'Sopa'
},
 {
   id:2,
   nombre:'Chatarra'
 },

 { 
   id:3,
   nombre: 'Postre'
},
 {
   id:4,
   nombre:'Segundo'
 }
];

private tipo_2: tipo2[] = [{

  id:1,
  codTipo:1,
  nombre:'Pollo'
},{
  id:2,
  codTipo:1,
  nombre:'Verduras'
},{
  id:3,
  codTipo:1,
  nombre:'Fideo'
},{
  id:4,
  codTipo:1,
  nombre:'Arroz'
},{
  id:5,
  codTipo:2,
  nombre:'hamburguesa'
},{
  id:6,
  codTipo:2,
  nombre:'hamburguesa'
},{
  id:7,
  codTipo:2,
  nombre:'hamburguesa'
},{
  id:8,
  codTipo:3,
  nombre:'hamburguesa'
},{
  id:9,
  codTipo:3,
  nombre:'hamburguesa'
},{
  id:10,
  codTipo:3,
  nombre:'hamburguesa'
},{
  id:11,
  codTipo:3,
  nombre:'hamburguesa'
},{
  id:12,
  codTipo:3,
  nombre:'hamburguesa'
},{
  id:13,
  codTipo:3,
  nombre:'hamburguesa'
},{
  id:14,
  codTipo:4,
  nombre:'hamburguesa'
},{
  id:15,
  codTipo:4,
  nombre:'hamburguesa'
},{
  id:16,
  codTipo:4,
  nombre:'hamburguesa'
},{
  id:17,
  codTipo:4,
  nombre:'hamburguesa'
},{
  id:18,
  codTipo:4,
  nombre:'hamburguesa'
},


];
private tipo_3: tipo3[] = []

getTipoOne():tipo1[]{
  return this.tipo_1;
}

getTipoDos():tipo2[]{
  return this.tipo_2;
}

  constructor() { }
}
