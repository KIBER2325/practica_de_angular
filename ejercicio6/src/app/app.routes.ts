import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { PaginasComponent } from "./components/paginas/paginas.component";
import { PromocionesComponent } from "./components/promociones/promociones.component";


//sidebar
import { Menu1Component } from "./components/menu1/menu1.component";
import { Menu2Component } from "./components/menu2/menu2.component";
import { Menu3Component } from "./components/menu3/menu3.component";
import { Menu4Component } from "./components/menu4/menu4.component";
import { Menu5Component } from "./components/menu5/menu5.component";


const APP_ROUTES: Routes = [
{path: 'home', component:HomeComponent},
{path: 'paginas',component:PaginasComponent},
{path: 'promociones',component:PromocionesComponent},
{path: 'menu1',component:Menu1Component},
{path: 'menu2',component:Menu2Component},
{path: 'menu3',component:Menu3Component},
{path: 'menu4',component:Menu4Component},
{path: 'menu5',component:Menu5Component},
{path: '**', pathMatch:'full', redirectTo:'home'}

]
 export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);