import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SiteBarComponent } from './components/site-bar/site-bar.component';
import { HomeComponent } from './components/home/home.component';
import { PaginasComponent } from './components/paginas/paginas.component';
import { PromocionesComponent } from './components/promociones/promociones.component';
import { APP_ROUTING } from './app.routes';
import { Menu1Component } from './components/menu1/menu1.component';
import { Menu2Component } from './components/menu2/menu2.component';
import { Menu3Component } from './components/menu3/menu3.component';
import { Menu4Component } from './components/menu4/menu4.component';
import { Menu5Component } from './components/menu5/menu5.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SiteBarComponent,
    HomeComponent,
    PaginasComponent,
    PromocionesComponent,
    Menu1Component,
    Menu2Component,
    Menu3Component,
    Menu4Component,
    Menu5Component
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
