import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DfullstackComponent } from './dfullstack.component';

describe('DfullstackComponent', () => {
  let component: DfullstackComponent;
  let fixture: ComponentFixture<DfullstackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DfullstackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DfullstackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
