import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DFrondEndComponent } from './dfrond-end.component';

describe('DFrondEndComponent', () => {
  let component: DFrondEndComponent;
  let fixture: ComponentFixture<DFrondEndComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DFrondEndComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DFrondEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
