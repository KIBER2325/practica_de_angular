import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'especialidades',
   loadChildren:() => import('./especialidades/especialidades.module').then(m => m.EspecialidadesModule
    ),},

    {path:'carreras',
    loadChildren:() => import('./carreras/carreras.module').then(m =>m.CarrerasModule), },
   
   {path:'marcas-delaptop',
    loadChildren:() => import('./marcas-delapto/marcas-delapto.module').then(m =>m.MarcasDelaptoModule)},
   
    {path:'marcas-derefresco',
    loadChildren:()=> import('./marcas-derefrescos/marcas-derefrescos.module').then(m => m.MarcasDerefrescosModule)
   },
  
   {path:'**',redirectTo:'especialidades'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
