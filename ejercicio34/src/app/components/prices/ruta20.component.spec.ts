import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Ruta20Component } from './ruta20.component';

describe('Ruta20Component', () => {
  let component: Ruta20Component;
  let fixture: ComponentFixture<Ruta20Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Ruta20Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Ruta20Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
