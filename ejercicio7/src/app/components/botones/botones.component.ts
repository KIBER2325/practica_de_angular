/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-botones',
  templateUrl: './botones.component.html',
  styleUrls: ['./botones.component.css']
})
export class BotonesComponent implements OnInit {
  opcion: number = 0;
  constructor() { }
  

  ngOnInit(): void {
  }


  ninguno():void{
    console.log("No se presiono ningun boton");
    this.opcion = 0
  }
   
  btn1():void{
    this.opcion = 1;
  }
  btn2():void{
    this.opcion = 2;
  }
  btn3():void{
    this.opcion = 0;
  } 
  
}
