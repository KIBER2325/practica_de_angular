import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  Objeto1 = {
    marca: "toyota",
    aparicion: "1937",
    ciudad:"japon"
  };

  Objeto2 = {
    color: 'blanco',
    marca: 'apple',
    precio: '1000 dolares'
  }
  
  constructor() { }

  ngOnInit(): void {
  }
}
