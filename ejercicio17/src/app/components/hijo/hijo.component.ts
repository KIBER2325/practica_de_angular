
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {



  constructor() { }
  
  mensaje1: string = 'Mensaje 1 del hijo ';
  mensaje2: string = 'mensaje 2 del hijo';
  mensaje3: string = 'mensaje 3 del hijo';
  mensaje4: string = 'mensaje 4 del hijo';
  mensaje5: string = 'mensaje 5 del hijo';

  @Output() EventoMensaje1 = new EventEmitter<string>();
  @Output() EventoMensaje2 = new EventEmitter<string>();
  @Output() EventoMensaje3 = new EventEmitter<string>();
  @Output() EventoMensaje4 = new EventEmitter<string>();
  @Output() EventoMensaje5 = new EventEmitter<string>();

  ngOnInit(): void {
    this.EventoMensaje1.emit(this.mensaje1);
    this.EventoMensaje2.emit(this.mensaje2)
    this.EventoMensaje3.emit(this.mensaje3)
    this.EventoMensaje4.emit(this.mensaje4)
    this.EventoMensaje5.emit(this.mensaje5)
  }

}
