import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  variable1 = 'mensaje uno ';
  variable2 = 'mensaje dos';
  variable3 = 'mensaje tres';
  variable4 = 'mensaje cuatro';
  variable5 = 'mensaje cinco.'
  
  constructor() { }

  ngOnInit(): void {
  }

}
