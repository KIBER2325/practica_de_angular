import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  Ipersona = {
    nombre: 'Kiber',
    apellido: 'Mena',
  }

  Iplaneta ={
    planetas: 2,
    planeta: 'jupiter'
  }

  constructor() { }
}
