import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  

  variable1: string = 'Primer variable del componente padre';
  variable2: string = 'Segunda variable del componente padre'
  objeto1: any;
  objeto2: any;

  //outputs
  mensaje1!: string;
  mensaje2!: string;
  json1!: any;
  json2!: any;

  constructor(public _dataservice: DataService) { }

  ngOnInit(): void {
    this.objeto1 = this._dataservice.Ipersona
    this.objeto2 = this._dataservice.Iplaneta
  }

  recibirMensaje($event: string): void{
    this.mensaje1 = $event;
    console.log(this.mensaje1);   
  }

  recibirM2($event: string): void{
    this.mensaje2 = $event;
    console.log(this.mensaje2);
  }

  recibirJson($event: any): void{
    this.json1 = $event
  }

  recibirJson2($event: any): void{
    this.json2 = $event
  }
}
