import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {


  @Input() primerMensaje!: string;
  @Input() segundMensaje!: string;
  @Input() primerObjeto!: any;
  @Input() segundoObjeto!: any;

  //Output
  mensaje: string = 'Mensaje del componente hijo al componente padre';
  @Output() EventoMensaje = new EventEmitter<string>()

  mensaje2: string = 'segundo Mensaje del hijo al padre';
  @Output() Evento2 = new EventEmitter<string>()

  jsonHijo: any = {
    empresa1: 'sonny',
    empresa2: 'apple',
    empresa3: 'huawei'
  }
  @Output() Eventojson1 = new EventEmitter<string>()

  jsonHijo2: any = {
    marca1: 'nikke',
    marca2: 'adidas',
    marca3: 'puma'
  }
  @Output() Eventojson2 = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.EventoMensaje.emit(this.mensaje)
    this.Evento2.emit(this.mensaje2)
    this.Eventojson1.emit(this.jsonHijo)
    this.Eventojson2.emit(this.jsonHijo2)
  }

}
